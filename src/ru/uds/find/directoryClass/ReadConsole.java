package ru.uds.find.directoryClass;

import ru.uds.find.directoryInterface.Read;

import java.util.Scanner;

public class ReadConsole implements Read {
    @Override
    public String read() {
        return new Scanner(System.in).nextLine();
    }
}

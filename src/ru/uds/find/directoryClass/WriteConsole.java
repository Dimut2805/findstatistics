package ru.uds.find.directoryClass;

import ru.uds.find.directoryInterface.Write;

public class WriteConsole implements Write {
    @Override
    public void write(String text) {
        System.out.println(text);
    }
}

package ru.uds.find.directoryClass;


import ru.uds.find.directoryInterface.Read;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class ReadFile implements Read {
    String url;

    ReadFile(String url) {
        this.url = url;
    }

    /**
     * Читает из файла исходный текст
     *
     * @return исходный текст
     */
    @Override
    public String read() {
        String string;
        String text = "";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(url))) {
            while ((string = bufferedReader.readLine()) != null) {
                if (!string.equals("")) text = text + (string + System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
}
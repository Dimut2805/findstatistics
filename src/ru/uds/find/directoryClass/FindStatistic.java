package ru.uds.find.directoryClass;


import java.util.Arrays;

/**
 * Ищет статистику в тексте
 * author D. Utin 17IT18
 */
public class FindStatistic {
    /**
     * Считает кол-во символов в тексте с пробелами
     *
     * @param stringText исходный текст
     * @return кол-во символов с пробелами
     */
    public static int countSymbolWithSpace(String stringText) {
        return stringText.length() -
                (stringText.split(System.lineSeparator()).length *
                        System.lineSeparator().length());
    }

    /**
     * Считает кол-во символов без пробелов в исходном тексте
     *
     * @param stringText исходный текст
     * @return кол-во символов без пробелов
     */
    public static int countSymbol(String stringText) {
        String[] arrayText = stringText
                .replaceAll(System.lineSeparator(), " ")
                .split("\\s+");
        return Arrays.stream(arrayText).mapToInt(String::length).sum();
    }

    /**
     * Считает кол-во слов в исходном тексте
     *
     * @param stringText исходный текст
     * @return кол-во слов в исходном тексте
     */
    public static int countWord(String stringText) {
        String[] arrayText = stringText.replaceAll(System.lineSeparator(), " ").split(" ");
        return (int) Arrays.stream(arrayText)
                .filter(FindStatistic::isWord)
                .count();
    }

    /**
     * Проверяет исходный текст на слова
     *
     * @param string исходный текст
     * @return слово ли, иль нет
     */
    private static boolean isWord(String string) {
        for (int i = 0; i < string.length(); i++) {
            if (Character.isDigit(string.charAt(i)) ||
                    Character.isLetter(string.charAt(i))) {
                return true;
            }
        }
        return false;
    }
}
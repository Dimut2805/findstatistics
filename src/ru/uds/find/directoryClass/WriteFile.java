package ru.uds.find.directoryClass;

import ru.uds.find.directoryInterface.Write;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFile implements Write {
    private String url;

    WriteFile(String url) {
        this.url = url;
    }

    /**
     * Записывает исходный текст в файл
     *
     * @param text текст для вывода в файл
     */
    @Override
    public void write(String text) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(url))) {
            bufferedWriter.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

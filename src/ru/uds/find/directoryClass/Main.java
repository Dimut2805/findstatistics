package ru.uds.find.directoryClass;

public class Main {
    public static void main(String[] args) {
        ReadFile readFile = new ReadFile("src\\ru\\uds\\find\\directoryFile\\input.txt");
        printFind(findFullStatistic(readFile.read()));
    }

    /**
     * Возвращает полную статистику из текста
     *
     * @param string исходный текст
     * @return полная статистика
     */
    public static String findFullStatistic(String string) {
        return "Кол-во символов(C пробелами): " + FindStatistic.countSymbolWithSpace(string) +
                System.lineSeparator() +
                "Кол-во символов(Без пробелов): " + FindStatistic.countSymbol(string) +
                System.lineSeparator() +
                "Кол-во слов: " + FindStatistic.countWord(string);
    }

    /**
     * Выводит полную статистику в консоль и файл
     *
     * @param string исходный текст
     */
    public static void printFind(String string) {
        new WriteFile("src\\ru\\uds\\find\\directoryFile\\output.txt").write(string);
        System.out.println(string);
    }
}